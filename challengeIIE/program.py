#!/usr/bin/env python3
import sys


def hops(start, end):
    jumps=0
    left=0
    right=0
    if start == end:
        return 0
    while(True):

        if end-start <= left+1:
            jumps+=1
            break
        else:
            distance=end-start
            if distance < (left+1+right+1):
                if distance < (left-1+right+1):
                    start+=(left-1)
                    left-=1
                    jumps+=1
                else:
                    jumps+=1
                    start+=left
            else:
                start+=(left+1)
                left+=1
                jumps+=1

        if end-start <= right+1:
            jumps+=1
            break
        else:
            distance=end-start
            if distance < (left+1+right+1):
                if distance < (left+1+right-1):
                    end-=(right-1)
                    right-=1
                    jumps+=1
                else:
                    jumps+=1
                    end-=right
            else:
                end-=(right+1)
                right+=1
                jumps+=1
    return jumps

if __name__ == '__main__':
    for line in sys.stdin:
        start,end = line.strip().split()
        answer = hops(int(start),int(end))
        print(start,"->",end,"takes",answer,"hops")
