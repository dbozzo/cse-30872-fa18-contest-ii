#!/usr/bin/env python3
import sys
import collections

def traverse(graph,start):
	frontier = [(start,start)]
	visited = {}
	while frontier:
		fr,to = frontier.pop()
		if to in visited:
			continue
		visited[to] = fr
		for neighbor in graph[to]:
			frontier.append((to, neighbor))
	return visited

cnt = 0
for line in sys.stdin:
	cnt += 1
	graph = collections.defaultdict(list)
	vertices = int(line.rstrip())
	edges = int(sys.stdin.readline().rstrip())
	for v in range(1,vertices+1):
		graph[v]
	for e in range(edges):
		node1,node2 = map(int,sys.stdin.readline().rstrip().split())
		graph[node1].append(node2)
		graph[node2].append(node1)
	result = []
	for n in graph:
		nodes = traverse(graph,n)
		result.append(sorted(nodes))
	result = sorted(list(set(tuple(row) for row in result)))

	if len(result)==1:
		print("Graph {} has {} group:".format(cnt,len(result)))
	else:
		print("Graph {} has {} groups:".format(cnt,len(result)))
	for group in result:
		print(" ".join(map(str,list(group))))
