#!/usr/bin/env python3
import sys

def getminpath(matrix,m, n):
    #initialize table
    table = [[(0, []) for i in range(n)] for j in range(m)]
    #make n moves across the table
    for col in range(n):
        #make decision for each table element
        for row in range(m):
            if col > 0:
                #this is a subsequent col, look at possible values and chose min
                #up left
                ul = row-1
                if ul < 0:
                    ul = m-1
                ul_val = table[ul][col-1][0]

                #left
                l = row
                l_val = table[l][col-1][0]

                #down left
                dl = row +1
                if dl > m-1:
                    dl = 0
                dl_val = table[dl][col-1][0]

                #this little section of if's is trash but it's how to ensure same output in the case
                # of equal cost paths 
                if ul_val == l_val and ul_val == dl_val:
                    #all three vals are same, pick the smallest row
                    if ul < l and ul< dl:
                        #ul chosen
                        new_list = [] + table[ul][col-1][1]
                        new_list.append(row+1)
                        table[row][col] = (table[ul][col-1][0] + matrix[row][col], new_list)
                    elif l < dl:
                        #l chosen
                        new_list = [] + table[l][col-1][1]
                        new_list.append(row+1)
                        table[row][col] = (table[l][col-1][0] + matrix[row][col], new_list)
                    else:
                        #dl
                        new_list = list(table[dl][col-1][1])
                        new_list.append(row+1)
                        table[row][col] = (table[dl][col-1][0] + matrix[row][col], new_list)
                    continue
                elif ul_val == l_val:
                    if ul < l:
                        #ul chosen
                        new_list = [] + table[ul][col-1][1]
                        new_list.append(row+1)
                        table[row][col] = (table[ul][col-1][0] + matrix[row][col], new_list)
                    else:
                        #l chosen
                        new_list = [] + table[l][col-1][1]
                        new_list.append(row+1)
                        table[row][col] = (table[l][col-1][0] + matrix[row][col], new_list)
                    continue
                elif l_val == dl_val:
                    if l < dl:
                        #l chosen
                        new_list = [] + table[l][col-1][1]
                        new_list.append(row+1)
                        table[row][col] = (table[l][col-1][0] + matrix[row][col], new_list)
                    else:
                        #dl
                        new_list = list(table[dl][col-1][1])
                        new_list.append(row+1)
                        table[row][col] = (table[dl][col-1][0] + matrix[row][col], new_list)
                    continue


                #normal decision making of what path to choose
                if ul_val < l_val and ul_val < dl_val:
                    # ul is chosen
                    new_list = [] + table[ul][col-1][1]
                    new_list.append(row+1)
                    table[row][col] = (table[ul][col-1][0] + matrix[row][col], new_list)
                elif l_val < dl_val:
                    #l is chosen
                    new_list = [] + table[l][col-1][1]
                    new_list.append(row+1)
                    table[row][col] = (table[l][col-1][0] + matrix[row][col], new_list)
                else:
                    #dl is chosen
                    new_list = list(table[dl][col-1][1])
                    new_list.append(row+1)
                    table[row][col] = (table[dl][col-1][0] + matrix[row][col], new_list)

            else:
                #this is the first col, value is just val
                table[row][col] = (matrix[row][col], [row+1])
    #print(table)
    #traverse through last column to find min val
    mintup = (float('inf'), [])
    for i in range(m):
        if table[i][n-1][0] < mintup[0]:
            mintup = table[i][n-1]
        
    
    return mintup

if __name__ == '__main__':
    lines = list(sys.stdin)
    while len(lines):
        m,n = map(int , lines.pop(0).strip().split())
        matrix = []
        for i in range(m):
            row = list(map(int, lines.pop(0).strip().split()))
            matrix.append(row)
        val, path = getminpath(matrix, m,n)
        print(val)
        print(' '.join(map(str, path)))

