#!/usr/bin/env python3
import sys
import string
import copy

class Graph:
	def __init__(self):
		self.vertexlist = []
		self.edgelist = []
	def addVertex(self, node):
		self.vertexlist.append(node)
	def addEdge(self, n1, n2):
		self.edgelist.append([n1,n2])
		self.edgelist.append([n2,n1])
	def path(self, start, end):
		visited = {v : False for v in self.vertexlist} 
		tmp = self.pathFinder(start, end, visited)
		allpaths = []
		for path in tmp:
			if path == sorted(path) and path not in allpaths:
				allpaths.append(path)
		return allpaths
	def pathFinder(self, begin, end, visited, p=None):
		visited[begin] = True 
		if p is None:
			p = []
		p = p + [begin]
		if begin == end: 
			return [p]
		pathlist = []
		for item in self.edgelist: 
			if begin == item[0] and not visited[item[1]]:
				newpath = self.pathFinder(item[1],end, copy.copy(visited), p)
				pathlist.extend(newpath)
		return pathlist

def check(s1, s2):
	m = len(s1)
	n = len(s2)
	if abs(m-n)>1: return False
	cnt = 0
	i,j = 0,0
	while i<m and j<n and cnt<=1:
		if s1[i] != s2[j]:
			if cnt ==1: return False
			if m>n:
				i += 1
			elif m<n:
				j += 1
			else:
				i+=1
				j+=1
			cnt += 1
		else:
			i+=1
			j+=1
	if i<m or j<n:
		cnt += 1
	return cnt == 1

g = Graph()
wordlst = []
for line in sys.stdin:
	g.addVertex(line.rstrip())
	wordlst.append(line.rstrip())

for word1 in wordlst:
	for word2 in wordlst:
		if check(word1,word2):
				g.addEdge(word1,word2)
result = []
for word1 in wordlst:
	for word2 in wordlst:
		result.append(g.path(word1, word2))	
maxlen = 0
for path in result:
	if path:
		for subpath in path:
			if len(subpath)>maxlen:
				maxlen = len(subpath)
				finalpath = subpath
print(len(finalpath))
for element in finalpath:
	print(element)
