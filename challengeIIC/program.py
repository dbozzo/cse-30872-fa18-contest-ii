#!/usr/bin/env python3


def permSequence(first, second):
    letters={}
    answer=[]
    for letter in first:
        if letter not in letters:
            letters[letter]=1
        else:
            letters[letter]+=1

    for letter in second:
        if letter in letters and letters[letter]>0:
            answer.append(letter)
            letters[letter]-=1
    answer.sort()
    return answer

if __name__ == '__main__':
    while(True):
        try:
            first=input().strip()
            second=input().strip()
        except EOFError:
            break

        answer = permSequence(first, second)
        print(''.join(answer))

    
        
