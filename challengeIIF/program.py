#!/usr/bin/env python3
import sys
import math

def compute(n, cals):
    miles = 0
    cals = sorted(cals)
    for i, cal in enumerate(reversed(cals)):
        curr = 2 ** (i) * cal
        miles += curr
    return miles


if __name__ == '__main__':
    
    n = input().strip()
    while 1:
        cals = input().strip().split()
        print(compute(int(n), list(map(int, cals))))

        #get next n
        try:
            n = input().strip()
        except Exception as ex:
            break

